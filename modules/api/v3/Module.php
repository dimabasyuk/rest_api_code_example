<?php

namespace app\modules\api\v3;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\api\v3\controllers';

    public function init()
    {
        parent::init();

    }
}
