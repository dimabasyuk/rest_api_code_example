<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\UserFavorites;
use app\models\UserFollowingStyles;
use app\models\YogaClasses;
use app\models\YogaClasses;
use app\models\YogaStyles;
use app\models\YogaStyles;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use dektrium\user\models\User;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\Response;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');


class YogaStylesController extends ActiveController
{
    public $modelClass = 'app\models\YogaStyles';



    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }

    /**
     * @api {get} /yoga-styles get all yoga styles by page
     * @apiDescription Get all yoga styles
     *
     * @apiName yoga_styles
     * @apiGroup Yoga Styles
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "seo,is_follow"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-styles
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionIndex($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");




        if ($full_list){

            $object_list = YogaStyles::find()
                ->orderBy(["sort_order"=>SORT_ASC])
                ->limit($page_size*$page);
        }
        else{

            $object_list = YogaStyles::find()
                ->orderBy(["sort_order"=>SORT_ASC])
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }



        return new ActiveDataProvider([
            'query' => $object_list,
            'pagination' => false,
        ]);



    }

    /**
     * @api {get} /yoga-styles/get_followed_styles get followed styles
     * @apiDescription get followed styles for user
     *
     * @apiName yoga_styles_get_followed_styles
     * @apiGroup Yoga Styles
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [user_id]  User ID for which you want to get the profile information. If this parameter is not
     * specified - the profile information will be returned for the current user
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /yoga-styles/get_followed_styles
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_followed_styles($user_id = -1)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");

        if ($user_id == -1){
            $cur_user =  User::findOne(['auth_key' => $access_token]);

            $user_id = $cur_user->id;
        }



        $follows = UserFollowingStyles::find()
            ->where("user_id=".$user_id)
            ->all();


        $user_ids = [];

        foreach($follows as $part){
            $user_ids[] = $part->style_id;
        }

        $users_list = YogaStyles::find()
            ->where(['in','id',$user_ids]);


        return new ActiveDataProvider([
            'query' => $users_list,
            'pagination' => false,
        ]);


    }

    /**
     * @api {get} /yoga-styles/follow_style follow style
     * @apiDescription Follow style
     *
     * @apiName yoga_styles_follow_style
     * @apiGroup Yoga Styles
     *
     * @apiParam {Number} style_id  Style id for which you want to follow
     * @apiParamExample {json} Request-Example:
     *     {
     *       "style_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /yoga-styles/follow_style
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFollow_style($style_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkIntParam($style_id, "style_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        //check if follow exist
        $is_follow_yet = UserFollowingStyles::find()
            ->where("user_id=".$cur_user->id." AND style_id=".$style_id)
            ->one();

        if (!isset($is_follow_yet)){
            //create link
            $user_follow_teacher = new UserFollowingStyles();
            $user_follow_teacher->user_id = $cur_user->id;
            $user_follow_teacher->style_id = $style_id;
            $user_follow_teacher->save();
            die;
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already followed for this style");
        }



    }

    /**
     * @api {get} /yoga-styles/unfollow_style unfollow style
     * @apiDescription Unfollow style
     *
     * @apiName yoga_styles_unfollow_style
     * @apiGroup Yoga Styles
     *
     * @apiParam {Number} style_id  Style id for which you want to unfollow
     * @apiParamExample {json} Request-Example:
     *     {
     *       "style_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /yoga-styles/unfollow_style
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionUnfollow_style($style_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkIntParam($style_id, "style_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if follow exist
        $is_follow_yet = UserFollowingStyles::find()
            ->where("user_id=".$cur_user->id." AND style_id=".$style_id)
            ->one();

        if (isset($is_follow_yet)){
            $is_follow_yet->delete();
            die;
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already unfollowed for this style");
        }


    }





    /**
     * @api {get} /yoga-styles/get_style_classes_by_page get style classes by page
     * @apiDescription Get style classes
     *
     * @apiName yoga_styles_get_style_classes_by_page
     * @apiGroup Yoga Styles
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [sort_type]  Sort type: "new" or "popular"
     * @apiParam {Number} style_id  Style id for which you want to get data
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "style,goals",
     *       "style_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-styles/get_style_classes_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_style_classes_by_page($page=1, $full_list=0, $page_size=10, $sort_type="new", $style_id)
    {


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $sort_type = App::setDefaultIfEmpty($sort_type,"new");


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($style_id, "style_id", true);


        $order = "create_date DESC";
        if ($sort_type != "new"){
            $order = "view_count DESC";
        }


        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where("main_yoga_style=".$style_id)
                ->orderBy($order)
                ->limit($page_size*$page);


        }
        else{
            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where("main_yoga_style=".$style_id)
                ->orderBy($order)
                ->offset($page_size*($page-1))
                ->limit($page_size);

        }

        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);



    }



    /**
     * @api {get} /yoga-styles/find_by_seo_url find by seo url
     * @apiDescription Find style by seo url
     *
     * @apiName yoga_styles_find_by_seo_url
     * @apiGroup Yoga Styles
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {String} url  Teacher seo url
     * @apiParamExample {json} Request-Example:
     *     {
     *       "url": "kundaliniyoga"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /yoga-styles/find_by_seo_url
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFind_by_seo_url($url){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $class_list = YogaStyles::find()
            ->where("seo_url='".$url."'");


        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);



    }


    /**
     * @api {get} /yoga-styles/get_page_count  get page count
     * @apiDescription Get style yoga classes page count. Can use for calculate page count for new and popular style
     * yoga classes
     *
     * @apiName yoga_styles_get_page_count
     * @apiGroup Yoga Styles
     *
     * @apiParam {Number} page_size Count of objects per page
     * @apiParam {Number} style_id Style id
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-styles/get_page_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_page_count($page_size=10, $style_id){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($style_id, "style_id", true);

        $total = YogaClasses::find()->andWhere("is_published=1")
            ->where("main_yoga_style=".$style_id)
            ->count();


        $page_count = ceil($total/$page_size);


        $return_obj = new \stdClass();
        $return_obj->page_count = $page_count;

        return $return_obj;

    }

    /**
     * @api {get} /yoga-styles/get_by_id get yoga style by id
     * @apiDescription Get yoga style by id
     *
     * @apiName yoga_styles_get_by_id
     * @apiGroup Yoga Styles
     *
     * @apiParam {Number} style_id  yoga style id id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "style_id": 16
     *     }
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-styles/get_by_id
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_by_id($style_id){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($style_id, "style_id", true);

        $object_list = YogaStyles::find()
            ->where("id=".$style_id);

        $object_count = YogaStyles::find()
            ->where("id=".$style_id)
            ->count();

        if ($object_count == 0){
            throw new YogaHttpException(404,"Yoga Style not found");
        }


        return new ActiveDataProvider([
            'query' => $object_list,
            'pagination' => false,
        ]);

    }



}

