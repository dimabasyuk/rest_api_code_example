<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\Badges;
use app\models\ChallengeStat;
use app\models\ClassClass;
use app\models\ClassComments;
use app\models\ClassGoals;
use app\models\ClassReminder;
use app\models\ClassTags;
use app\models\CollectionClasses;
use app\models\Collections;
use app\models\Goals;
use app\models\Radio;
use app\models\Slider;
use app\models\Tags;
use app\models\UserBadges;
use app\models\UserBonusStat;
use app\models\UserFavorites;
use app\models\UserWatched;
use app\models\UserWatchStat;
use app\models\UserWatchSummary;
use app\models\UserWishlist;
use app\models\YogaClasses;
use app\models\YogaClasses;
use app\models\YogaStyles;
use dektrium\user\models\Profile;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use dektrium\user\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\rest\ActiveController;
use yii\web\Response;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');



class YogaClassesController extends ActiveController
{

    public $modelClass = 'app\models\YogaClasses';



    /**
     * @api {get} /yoga-classes/add_watch_stat add watch statistic
     * @apiDescription Add watch statistic. <b>Call the function after watching each video</b>
     *
     * @apiName yoga_classes_add_watch_stat
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} class_id  Yoga Class id
     * @apiParam {Number} watch_seconds  Watched seconds
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 55,
     *       "watch_seconds": 62
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/add_watch_stat
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionAdd_watch_stat($class_id,$watch_seconds)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($class_id, "class_id",true);
        YogaHttpException::checkIntParam($watch_seconds, "watch_seconds",true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        $is_full_watch = false;



        //add user watch stat
        $is_watch_yet = new UserWatchStat();
        $is_watch_yet->user_id = $cur_user->id;
        $is_watch_yet->class_id = $class_id;
        $is_watch_yet->watch_begin = time()-$watch_seconds;
        $is_watch_yet->watch_end = time();
        $is_watch_yet->watch_token = "mob_".uniqid();
        $is_watch_yet->save();



        $watch_amount = $watch_seconds;

        $watched_class = YogaClasses::find()->where("is_published=1")->andWhere("id=".$class_id)->one();

        if (!isset($watched_class)){
            throw new YogaHttpException(404,"Yoga Class not found");
        }

        $watched_class->view_count++;
        $watched_class->save(false);

        if ($watch_amount > $watched_class->duration*60){
            $watch_amount = $watched_class->duration*60;
        }

        if ($watch_amount >=($watched_class->duration*60*0.8) ){
            $is_full_watch = true;
        }
        //180 min block
        if ($watch_amount > 10800){
            $watch_amount = 10800;
        }

        // Add to watched list
        $is_watch_yet = UserWatched::find()->where("user_id=".$cur_user->id." AND class_id=".$class_id)->one();

        if (!isset($is_watch_yet)){
            //create link
            $user_watch = new UserWatched();
            $user_watch->user_id = $cur_user->id;
            $user_watch->class_id = $class_id;
            $user_watch->watched_date = date("Y-m-d H:i:s");
            $user_watch->is_full_watch = $is_full_watch;
            $user_watch->save();
        }

        // check if wishlist exist
        $is_wishlist_yet = UserWishlist::find()->where("user_id=".$cur_user->id." AND class_id=".$class_id)->one();
        if (isset($is_wishlist_yet)){
            $is_wishlist_yet->delete();
        }

        //----------Add stat summary-------
        // check if wishlist exist
        $is_watch_yet = UserWatchSummary::find()->where("user_id=".$cur_user->id." AND date='".date('Y-m-d')."'")->one();
        if (isset($is_watch_yet)){
            //create link
            //180 min block
            if ($is_watch_yet->watch_seconds + $watch_amount > 10800){
                $watch_amount = 0;
            }
            $is_watch_yet->watch_seconds += $watch_amount;
            $is_watch_yet->watch_minutes = round($is_watch_yet->watch_seconds/60);
            $is_watch_yet->save();

        }
        else{
            $is_watch_yet = new UserWatchSummary();
            $is_watch_yet->user_id = $cur_user->id;
            $is_watch_yet->date = date('Y-m-d');
            $is_watch_yet->watch_seconds = $watch_amount;
            $is_watch_yet->watch_minutes = round($is_watch_yet->watch_seconds/60);
            $is_watch_yet->save();
        }

        //check challenges
        $user_challenges = $cur_user->followChallenges;

        foreach($user_challenges as $chall){

            if (($chall->dayToStart>0) || ($chall->dayToFinish<0)) continue;

            $cur_chall_stat = ChallengeStat::find()->where("user_id=".$cur_user->id." AND challenge_id=".$chall->id)->one();

            if (isset($cur_chall_stat)){
                $cur_chall_stat->watch_seconds+= $watch_amount;
                $cur_chall_stat->save();
            }
            else{
                $new_cur_chall_stat = new ChallengeStat();
                $new_cur_chall_stat->user_id = $cur_user->id;
                $new_cur_chall_stat->challenge_id = $chall->id;
                $new_cur_chall_stat->watch_seconds = $watch_amount;
                $new_cur_chall_stat->save();
            }

        }


        //!gamification


        //check if watch full video
        if ($is_full_watch){
            //add karma
            $cur_user->profile->karma+=$watched_class->karma_points;
            $cur_user->profile->save();

            //check if more one full video per day
            $bonus_user_stat = UserBonusStat::find()->where("user_id=".$cur_user->id)->one();

            if (isset($bonus_user_stat)){

                if ($bonus_user_stat->date == date("Y-m-d")){
                    $bonus_user_stat->full_watch_today ++;
                    $bonus_user_stat->save(false);

                    $cur_user->profile->karma+=30;
                    $cur_user->profile->save();
                }
                else{
                    //if in row
                    $yeaterday_date = date("Y-m-d", strtotime("yesterday"));
                    if ($bonus_user_stat->date ==  $yeaterday_date){
                        $bonus_user_stat->watch_in_row++;
                        $bonus_user_stat->save(false);


                        //add karma
                        switch ($bonus_user_stat->watch_in_row) {
                            case 3:
                                $cur_user->profile->karma+=20;
                                $cur_user->profile->save();
                                break;
                            case 5:
                                $cur_user->profile->karma+=30;
                                $cur_user->profile->save();
                                break;
                            case 7:
                                $cur_user->profile->karma+=40;
                                $cur_user->profile->save();
                                break;
                            case 10:
                                $cur_user->profile->karma+=50;
                                $cur_user->profile->save();
                                break;
                            case 15:
                                $cur_user->profile->karma+=60;
                                $cur_user->profile->save();
                                break;
                            case 30:
                                $cur_user->profile->karma+=70;
                                $cur_user->profile->save();
                                break;
                            case 60:
                                $cur_user->profile->karma+=80;
                                $cur_user->profile->save();
                                break;
                            case 90:
                                $cur_user->profile->karma+=90;
                                $cur_user->profile->save();
                                break;
                            case 120:
                                $cur_user->profile->karma+=100;
                                $cur_user->profile->save();
                                break;
                            case 365:
                                $cur_user->profile->karma+=110;
                                $cur_user->profile->save();
                                break;
                        }
                    }
                    else{
                        $bonus_user_stat->watch_in_row = 1;
                        $bonus_user_stat->save(false);
                    }
                }



            }else{
                $bonus_user_stat_new = new UserBonusStat();
                $bonus_user_stat_new->full_watch_today = 1;
                $bonus_user_stat_new->watch_in_row = 1;
                $bonus_user_stat_new->user_id = $cur_user->id;
                $bonus_user_stat->date = date("Y-m-d");
                $bonus_user_stat->save(false);
            }
        }




        die;

    }



    /**
     * @api {get} /yoga-classes/get_related get related yoga classes
     * @apiDescription Get related yoga classes
     *
     * @apiName yoga_classes_get_related
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} class_id  Yoga Class id
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 55
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_related
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_related($class_id)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $part_rating = ClassClass::find()->where("class1=".$class_id)->all();
        $class_ids = [];

        foreach($part_rating as $part){
            $class_ids[] = $part->class2;
        }


        $class_list = YogaClasses::find()->andWhere("is_published=1")
            ->where(['in','id',$class_ids]);


        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);


    }



    /**
     * @api {get} /yoga-classes/get_students_by_page get students by page
     * @apiDescription Get students by page
     *
     * @apiName yoga_classes_get_students_by_page
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [sort_type]  Sort type: "asc" or "desc"
     * @apiParam {Number} class_id  Yoga Class id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 55
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_students_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_students_by_page($page=1, $full_list=0, $page_size=10, $sort_type = "asc", $class_id)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $sort_type = App::setDefaultIfEmpty($sort_type,"asc");


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $sort_int = SORT_ASC;
        if ( strtolower($sort_type) == "desc") {
            $sort_int = SORT_DESC;
        }


        $watch_list = UserWatched::find()
            ->where("class_id=".$class_id)
            ->orderBy(['id'=>$sort_int])
            ->all();


        $students_id_array = [];
        foreach($watch_list as $stydent){
            $students_id_array[] = $stydent->user_id;
        }

        //for sort in same order as in "IN Array"
        $exp = new Expression('FIELD (user_id,' . implode(',', $students_id_array) . ')');

        if ($full_list){

            $object_list = Profile::find()
                ->where(['in','user_id',$students_id_array])
                ->orderBy([$exp])
                ->limit($page_size*$page);
        }
        else{

            $object_list = Profile::find()
                ->where(['in','user_id',$students_id_array])
                ->orderBy([$exp])
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $object_list,
            'pagination' => false,
        ]);

    }


    /**
     * @api {get} /yoga-classes/get_comments_by_page get comments by page
     * @apiDescription Get yoga class comments by page
     *
     * @apiName yoga_classes_get_comments_by_page
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [sort_type]  Sort type: "asc" or "desc"
     * @apiParam {Number} class_id  Yoga Class id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 55
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_comments_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_comments_by_page($page=1, $full_list=0, $page_size=10,  $sort_type = "asc", $class_id){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $sort_type = App::setDefaultIfEmpty($sort_type,"asc");


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $sort_int = SORT_ASC;
        if ( strtolower($sort_type) == "desc") {
            $sort_int = SORT_DESC;
        }


        if ($full_list){

            $comment_list = ClassComments::find()
                ->where("comment_type=0 AND class_id=".$class_id)
                ->orderBy(['id'=>$sort_int])
                ->limit($page_size*$page)
                ->all();

        }
        else{
            $comment_list = ClassComments::find()
                ->where("comment_type=0 AND class_id=".$class_id)
                ->orderBy(['id'=>$sort_int])
                ->offset($page_size*($page-1))
                ->limit($page_size)
                ->all();
        }

        foreach ($comment_list as $comment){


            $comment_user = Profile::find()->where("user_id=".$comment->author_id)->one();
            $comment->username = $comment_user->first_name." ".$comment_user->last_name;
            $comment->address = $comment_user->address;
            $comment->ava = $comment_user->ava_photo;
            $comment->practice_since = $comment_user->practice_since;
        }

        return $comment_list;

    }


    /**
     * @api {get} /yoga-classes/add_comment add comment
     * @apiDescription Add comment to yoga classes
     *
     * @apiName yoga_classes_add_comment
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} class_id  Yoga Class id
     * @apiParam {Number} comment_text  Comment text
     * @apiParam {Number} rating  user rating of yoga class in range from 0 to 5
     * @apiParam {Number} [parent_id]  Comment parent id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 55
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "comment_id": 801
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/add_comment
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionAdd_comment($class_id,$comment_text,$parent_id=0, $rating=5){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $parent_id = App::setDefaultIfEmpty($parent_id,0);
        $rating = App::setDefaultIfEmpty($rating,5);


        //check params
        YogaHttpException::checkIntParam($class_id, "class_id", true);
        YogaHttpException::checkRequiredParam($comment_text, "comment_text");
        YogaHttpException::checkRangeParam($rating, "rating",0,5,true);
        YogaHttpException::checkIntParam($parent_id, "parent_id");



        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);


        $cur_class = YogaClasses::find()->where("is_published=1")->andWhere("id = ".$class_id)->one();

        if (!isset($cur_class)){
            throw new YogaHttpException(404,"Yoga class not found");
        }


        $transaction = \Yii::$app->db->beginTransaction();

        try{

            $new_comment = new ClassComments();
            $new_comment->class_id = $class_id;
            $new_comment->parent_id = $parent_id;
            $new_comment->author_id = $cur_user->id;
            $new_comment->like_count = 0;
            $new_comment->text = $comment_text;
            $new_comment->create_date = date("Y-m-d H:i:s");
            $new_comment->comment_type=0;
            $new_comment->rating=$rating;
            $new_comment->save();

            // add rating to class
            if ($rating != 0){

                $cur_class->sum_review_rating += $rating;
                $cur_class->review_count +=1;

                $mark = $rating."";

                switch ($mark) {
                    case "5":
                        $cur_class->five_stars_count++;
                        break;
                    case "4":
                        $cur_class->four_stars_count++;
                        break;
                    case "3":
                        $cur_class->three_stars_count++;
                        break;
                    case "2":
                        $cur_class->two_stars_count++;
                        break;
                    case "1":
                        $cur_class->one_stars_count++;
                        break;
                }

                $cur_class->save();
            }


            $transaction->commit();


        }catch(Exception $e) {
            throw new YogaHttpException(417,"db transaction error");
        }

        $return_object = new \stdClass();
        $return_object->comment_id = $new_comment->id;

        return $return_object;

    }


    /**
     * @api {get} /yoga-classes/get_featured_by_page get featured by page
     * @apiDescription Get featured yoga classes by page
     *
     * @apiName yoga_classes_get_featured_by_page
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [rand]  Show in random order: 1 - yes, 0 - no
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 55
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_featured_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_featured_by_page($page=1, $full_list=0, $page_size=10, $rand=1 ){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $rand = App::setDefaultIfEmpty($rand,1);


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkRangeParam($rand, "rand", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            if ($rand){
                $class_list = YogaClasses::find()->andWhere("is_published=1")
                    ->where("show_in_slider=1")
                    ->orderBy("RAND()")
                    ->limit($page_size*$page);
            }
            else{
                $class_list = YogaClasses::find()->andWhere("is_published=1")
                    ->where("show_in_slider=1")
                    ->orderBy("slider_position")
                    ->limit($page_size*$page);
            }
        }
        else{

            if ($rand){
                $class_list = YogaClasses::find()->andWhere("is_published=1")
                    ->where("show_in_slider=1")
                    ->orderBy("RAND()")
                    ->offset($page_size*($page-1))
                    ->limit($page_size);
            }
            else{
                $class_list = YogaClasses::find()->andWhere("is_published=1")
                    ->where("show_in_slider=1")
                    ->orderBy("slider_position")
                    ->offset($page_size*($page-1))
                    ->limit($page_size);
            }

        }



        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);



    }


    /**
     * @api {get} /yoga-classes/filter filter
     * @apiDescription Filter yoga classes by new/popular, goals, levels, duration, teacher id,  style id
     *
     * @apiName yoga_classes_filter
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {String} [filter_type]  Filter type: "new" or "popular" or "watch_later" or "favorites"  or "tinder" or "history"
     * @apiParam {String} [goals[]]   goal id for filter. If you want filter by several goals - send same
     * params with other value. See Request-Example.
     * @apiParam {String} [levels[]] levels id for filter. If you want filter by several levels - send same
     * params with other value. See Request-Example.
     * @apiParam {String} [duration[]]  duration id for filter. Available values: 10,20,30,45,60. If you want filter by
     * several durations - send same params with other value. See Request-Example.
     * @apiParam {Number} [teacher_id] teacher id
     * @apiParam {Number} [style_id]  style id
     * @apiParam {Number} [collection_id]  collection id
     * @apiParam {Number} [search]  search query
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "seo",
     *       "filter_type": "popular",
     *       "levels[]": 2,
     *       "levels[]": 3,
     *       "duration[]": 20,
     *       "duration[]": 45,
     *       "goals[]": 1,
     *       "goals[]": 5,
     *       "goals[]": 10,
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/filter
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFilter(){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $filter_type = App::getRequestData("filter_type",false);
        $page = App::getRequestData("page",false);
        $page_size = App::getRequestData("page_size",false);
        $full_list = App::getRequestData("full_list",false);

        $goals = App::getRequestData("goals",false);
        $levels =  App::getRequestData("levels",false);
        $duration =  App::getRequestData("duration",false);
        $teacher_id =  App::getRequestData("teacher_id",false);
        $style_id = App::getRequestData("style_id",false);
        $collection_id = App::getRequestData("collection_id",false);
        $search = App::getRequestData("search",false);


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $filter_type = App::setDefaultIfEmpty($filter_type,"new");

        if($goals == "") unset($goals);
        if($levels == "") unset($levels);
        if($duration == "") unset($duration);
        if($teacher_id == "") unset($teacher_id);
        if($style_id == "") unset($style_id);
        if($collection_id == "") unset($collection_id);
        if($search == "") unset($search);


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($teacher_id, "teacher_id");
        YogaHttpException::checkIntParam($style_id, "style_id");
        YogaHttpException::checkIntParam($collection_id, "collection_id");


        $cur_user =  User::findOne(['auth_key' => $access_token]);
        $user_id = $cur_user->id;


        $where_query = "";

        // ---------- Filter goals ----------
        if ((isset($goals)) && (count($goals)>0) && ($goals[0]!="")){
            $where_query.= "t_class_goals.class_id = t_yoga_classes.id AND t_class_goals.goal_id IN (".implode(",", $goals).")";
        }


        // ---------- Filter search ----------
        if ((isset($search)) && ($search!="")){
            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $additional_brackets = false;
            //search by style
            $style_ids = [];
            $style_list_search = YogaStyles::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();

            $style_in_query = "";
            foreach($style_list_search as $style_search){

                $style_in_query .= "'".$style_search->id."', ";
                $style_ids[] = $style_search->id;
            }
            if (count($style_list_search) > 0){
                $style_in_query = substr($style_in_query, 0, -2);
                $style_in_query = "(".$style_in_query.")";
                $additional_brackets = true;
            }

            //search by teacher
            $teacher_in_query = "";
            $teacher_user_ids = [];
            $teacher_user_list = User::find()
                ->select(['id'])
                ->where("role = 2")->all();

            foreach($teacher_user_list as $user){
                $teacher_user_ids[] = $user->id;
            }

            $teacher_profile_list =  Profile::find()
                ->select(['user_id'])
                ->where("first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%'")
                ->andWhere(['in','user_id',$teacher_user_ids])
                ->all();

            foreach($teacher_profile_list as $profile_search){
                $teacher_in_query .= "'".$profile_search->user_id."', ";
                $teacher_ids[] = $profile_search->user_id;
            }


            if (count($teacher_profile_list) > 0){
                $teacher_in_query = substr($teacher_in_query, 0, -2);
                $teacher_in_query = "(".$teacher_in_query.")";
                $additional_brackets = true;
            }

            //search by tags
            $tags_in_query = "";
            $tag_ids = [];
            $tag_list = Tags::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
            foreach($tag_list as $tag){
                $tag_ids[] = $tag->id;
            }

            $class_tag_ids = [];
            $class_tag_list = ClassTags::find()
                ->select(['class_id'])
                ->where(['in','tag_id',$tag_ids])
                ->all();
            foreach($class_tag_list as $class_tag){
                $tags_in_query .= "'".$class_tag->class_id."', ";
                $class_tag_ids[] = $class_tag->class_id;
            }
            if (count($class_tag_list) > 0){
                $tags_in_query = substr($tags_in_query, 0, -2);
                $tags_in_query = "(".$tags_in_query.")";
                $additional_brackets = true;
            }

            //search by goals
            $goal_ids = [];
            $goal_in_query = "";
            $goal_list = Goals::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
            foreach($goal_list as $goal){
                $goal_ids[] = $goal->id;
            }

            $class_goal_ids = [];
            $class_goal_list = ClassGoals::find()
                ->select(['class_id'])
                ->where(['in','goal_id',$goal_ids])
                ->all();
            foreach($class_goal_list as $class_goal){
                $goal_in_query .= "'".$class_goal->class_id."', ";
                $class_goal_ids[] = $class_goal->class_id;
            }
            if (count($class_goal_list) > 0){
                $goal_in_query = substr($goal_in_query, 0, -2);
                $goal_in_query = "(".$goal_in_query.")";
                $additional_brackets = true;
            }




            if ($additional_brackets){
                $where_query.= "(";
            }


            $where_query.= " (t_yoga_classes.name LIKE '%".$search."%' OR t_yoga_classes.description LIKE '%".$search."%' OR t_yoga_classes.description_full LIKE '%".$search."%')";

            if (count($style_list_search) > 0){
                $where_query.= " OR (t_yoga_classes.main_yoga_style in ".$style_in_query.")";
            }

            if (count($teacher_profile_list) > 0){
                $where_query.= " OR (t_yoga_classes.teacher_id in ".$teacher_in_query.")";
            }

            if (count($class_tag_list) > 0){
                $where_query.= " OR (t_yoga_classes.id in ".$tags_in_query.")";
            }

            if (count($class_goal_list) > 0){
                $where_query.= " OR (t_yoga_classes.id in ".$goal_in_query.")";
            }

            if ($additional_brackets){
                $where_query.= ")";
            }
        }

        // ---------- Filter collection ----------

        if ((isset($collection_id)) && ($collection_id != "") && ($collection_id != "0")){

            $collection = CollectionClasses::find()->where("collection_id=".$collection_id)->count();

            if ($collection>0){
                if ($where_query != ""){
                    $where_query.=" AND ";
                }

                //search by style
                $collection_classes_ids = [];
                $collection_list_search = CollectionClasses::find()->select(['class_id'])->where("collection_id = ".$collection_id)->all();

                $collection_in_query = "";
                foreach($collection_list_search as $collection_search){

                    $collection_in_query .= "'".$collection_search->class_id."', ";
                    $collection_classes_ids[] = $collection_search->class_id;
                }
                if (count($collection_list_search) > 0){
                    $collection_in_query = substr($collection_in_query, 0, -2);
                    $collection_in_query = "(".$collection_in_query.")";
                }

                $where_query.= "  (t_yoga_classes.id in ".$collection_in_query.")";
            }


        }



        // ---------- Filter levels ----------


        if ((isset($levels)) && (count($levels)>0) && ($levels[0]!=0) && ($levels[0]!="")){



            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $where_query.=" (";
            for($j=0;$j<count($levels); $j++){

                if ($levels[$j]!=2){
                    $where_query.="t_yoga_classes.level LIKE '%".$levels[$j]."%'";
                }
                else{
                    $where_query.="t_yoga_classes.level LIKE '%".$levels[$j]."%' OR t_yoga_classes.level LIKE '1-3'";
                }

                if ($j != count($levels)-1){
                    $where_query.= "OR ";
                }

            }
            $where_query.=" )";

        }



        // ---------- Filter duration ----------
        if ((isset($duration)) && (count($duration)>0) && ($duration[0]!=0) && ($duration[0]!="")){

            if ($where_query != ""){
                $where_query.=" AND ";
            }


            $where_query.=" (";
            for($j=0;$j<count($duration); $j++){

                $duration_to = $duration[$j];
                $duration_from = $duration[$j]-10;
                if ($duration_from < 0) {
                    $duration_from = 0;
                }
                if ($duration_to>=60){
                    $duration_to = 99999;
                }

                $where_query.="(t_yoga_classes.duration > ".$duration_from." AND t_yoga_classes.duration <= ".$duration_to.") ";


//                if ($duration[$j]==10){
//                    $where_query.="t_yoga_classes.duration <= 10 ";
//                }
//                elseif ($duration[$j]==20){
//                    $where_query.="(t_yoga_classes.duration > 10 AND t_yoga_classes.duration <=20) ";
//                }
//                elseif ($duration[$j]==30){
//                    $where_query.="(t_yoga_classes.duration > 20 AND t_yoga_classes.duration <=30) ";
//                }
//                elseif ($duration[$j]==45){
//                    $where_query.="(t_yoga_classes.duration > 30 AND t_yoga_classes.duration <=45) ";
//                }
//                elseif ($duration[$j]==60){
//                    $where_query.="t_yoga_classes.duration > 45 ";
//                }
//                else{
//                    throw new YogaHttpException(400,"Duration available values: 10,20,30,45,60");
//                }




                if ($j != count($duration)-1){
                    $where_query.= "OR ";
                }

            }
            $where_query.=" )";



        }


        // ---------- Filter teacher ----------
        if ((isset($teacher_id)) && ($teacher_id != "") && ($teacher_id != "0")){

            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $where_query.= " teacher_id = ".$teacher_id;
        }

        // ---------- Filter style ----------
        if ((isset($style_id)) && ($style_id != "") && ($style_id != "0")){

            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $where_query.= " main_yoga_style = ".$style_id;
        }


        $total = 0;
        $class_list=null;

        // ---------- NEW ----------
        if ($filter_type == "new"){



            if ($where_query == ""){

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->orderBy("create_date DESC")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->orderBy("create_date DESC")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }
            else{

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }

        }
        // ---------- / NEW ----------



        // ---------- POPULAR ----------
        if ($filter_type == "popular"){

            if ($where_query == ""){


                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }

                usort($class_list, function($a, $b)
                {
                    return strcmp($b->view_count,$a->view_count);
                });


            }
            else{


                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }

                usort($class_list, function($a, $b)
                {
                    return strcmp($b->view_count,$a->view_count);
                });


            }

        }
        // ---------- / POPULAR ----------


        // ---------- WATCH LATER ----------
        if ($filter_type == "watch_later"){

            //get watch later list
            $objects_list = UserWishlist::find()->where("user_id=".$user_id)->all();
            $objects_id_array = [];
            foreach($objects_list as $obj){
                $objects_id_array[] = $obj->class_id;
            }

            if ($where_query == ""){

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }
            else{

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }

        }
        // ---------- / WATCH LATER ----------


        // ---------- FAVORITES ----------
        if ($filter_type == "favorites"){

            //get favorites list
            $objects_list = UserFavorites::find()->where("user_id=".$user_id)->all();
            $objects_id_array = [];
            foreach($objects_list as $obj){
                $objects_id_array[] = $obj->class_id;
            }

            if ($where_query == ""){

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }
            else{

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }

        }
        // ---------- / FAVORITES ----------


        // ---------- HISTORY ----------
        if ($filter_type == "history"){

            //get favorites list
            $objects_list = UserWatched::find()->where("user_id=".$user_id)->all();
            $objects_id_array = [];
            foreach($objects_list as $obj){
                $objects_id_array[] = $obj->class_id;
            }

            if ($where_query == ""){

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }
            else{

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }

        }
        // ---------- / HISTORY ----------

        // ---------- TINDER ----------
        if ($filter_type == "tinder"){

            //get favorites list
            $tinder_class_list = YogaClasses::find()->andWhere("is_published=1")
                ->select(['id'])
                ->orderBy("view_count DESC")
                ->all();

            $tinder_count = 0;
            $objects_id_array = [];

            foreach($tinder_class_list as $tinder_class){

                $is_watched = UserWatched::find()
                    ->where("class_id=".$tinder_class->id)
                    ->andWhere("user_id=".$user_id)
                    ->count();

                if ($is_watched == 0){
                    $objects_id_array[] = $tinder_class->id;
                    $tinder_count++;

                    if ($tinder_count == 10){
                        break;
                    }
                }
            }

            if ($where_query == ""){

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }
            else{

                if ($full_list){
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->limit($page_size*$page);
                }
                else{
                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->offset($page_size*($page-1))
                        ->limit($page_size);
                }


            }

        }
        // ---------- / TINDER ----------

        return  new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);



    }


    /**
     * @api {get} /yoga-classes/filter_page_count filter page count
     * @apiDescription Function same to Filter function, return page count
     *
     * @apiName yoga_classes_filter_page_count
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {String} [filter_type]  Filter type: "new" or "popular" or "watch_later" or "favorites"  or "tinder" or "history"
     * @apiParam {String} [goals[]]   goal id for filter. If you want filter by several goals - send same
     * params with other value. See Request-Example.
     * @apiParam {String} [levels[]] levels id for filter. If you want filter by several levels - send same
     * params with other value. See Request-Example.
     * @apiParam {String} [duration[]]  duration id for filter. Available values: 10,20,30,45,60. If you want filter by
     * several durations - send same params with other value. See Request-Example.
     * @apiParam {Number} [teacher_id] teacher id
     * @apiParam {Number} [style_id]  style id
     * @apiParam {Number} [collection_id]  collection id
     * @apiParam {Number} [search]  search query
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "seo",
     *       "filter_type": "popular",
     *       "levels[]": 2,
     *       "levels[]": 3,
     *       "duration[]": 20,
     *       "duration[]": 45,
     *       "goals[]": 1,
     *       "goals[]": 5,
     *       "goals[]": 10,
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/filter_page_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFilter_page_count(){
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $filter_type = App::getRequestData("filter_type",false);
        $page = App::getRequestData("page",false);
        $page_size = App::getRequestData("page_size",false);
        $full_list = App::getRequestData("full_list",false);

        $goals = App::getRequestData("goals",false);
        $levels =  App::getRequestData("levels",false);
        $duration =  App::getRequestData("duration",false);
        $teacher_id =  App::getRequestData("teacher_id",false);
        $style_id = App::getRequestData("style_id",false);
        $collection_id = App::getRequestData("collection_id",false);
        $search = App::getRequestData("search",false);


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $filter_type = App::setDefaultIfEmpty($filter_type,"new");



        if($goals == "") unset($goals);
        if($levels == "") unset($levels);
        if($duration == "") unset($duration);
        if($teacher_id == "") unset($teacher_id);
        if($style_id == "") unset($style_id);
        if($collection_id == "") unset($collection_id);
        if($search == "") unset($search);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($teacher_id, "teacher_id");
        YogaHttpException::checkIntParam($style_id, "style_id");
        YogaHttpException::checkIntParam($collection_id, "collection_id");


        $cur_user =  User::findOne(['auth_key' => $access_token]);
        $user_id = $cur_user->id;

        $where_query = "";

        // ---------- Filter goals ----------
        if ((isset($goals)) && (count($goals)>0) && ($goals[0]!="")){
            $where_query.= "t_class_goals.class_id = t_yoga_classes.id AND t_class_goals.goal_id IN (".implode(",", $goals).")";
        }

        // ---------- Filter search ----------
        if ((isset($search)) && ($search!="")){
            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $additional_brackets = false;
            //search by style
            $style_ids = [];
            $style_list_search = YogaStyles::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();

            $style_in_query = "";
            foreach($style_list_search as $style_search){

                $style_in_query .= "'".$style_search->id."', ";
                $style_ids[] = $style_search->id;
            }
            if (count($style_list_search) > 0){
                $style_in_query = substr($style_in_query, 0, -2);
                $style_in_query = "(".$style_in_query.")";
                $additional_brackets = true;
            }

            //search by teacher
            $teacher_in_query = "";
            $teacher_user_ids = [];
            $teacher_user_list = User::find()
                ->select(['id'])
                ->where("role = 2")->all();

            foreach($teacher_user_list as $user){
                $teacher_user_ids[] = $user->id;
            }

            $teacher_profile_list =  Profile::find()
                ->select(['user_id'])
                ->where("first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%'")
                ->andWhere(['in','user_id',$teacher_user_ids])
                ->all();

            foreach($teacher_profile_list as $profile_search){
                $teacher_in_query .= "'".$profile_search->user_id."', ";
                $teacher_ids[] = $profile_search->user_id;
            }


            if (count($teacher_profile_list) > 0){
                $teacher_in_query = substr($teacher_in_query, 0, -2);
                $teacher_in_query = "(".$teacher_in_query.")";
                $additional_brackets = true;
            }

            //search by tags
            $tags_in_query = "";
            $tag_ids = [];
            $tag_list = Tags::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
            foreach($tag_list as $tag){
                $tag_ids[] = $tag->id;
            }

            $class_tag_ids = [];
            $class_tag_list = ClassTags::find()
                ->select(['class_id'])
                ->where(['in','tag_id',$tag_ids])
                ->all();
            foreach($class_tag_list as $class_tag){
                $tags_in_query .= "'".$class_tag->class_id."', ";
                $class_tag_ids[] = $class_tag->class_id;
            }
            if (count($class_tag_list) > 0){
                $tags_in_query = substr($tags_in_query, 0, -2);
                $tags_in_query = "(".$tags_in_query.")";
                $additional_brackets = true;
            }

            //search by goals
            $goal_ids = [];
            $goal_in_query = "";
            $goal_list = Goals::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
            foreach($goal_list as $goal){
                $goal_ids[] = $goal->id;
            }

            $class_goal_ids = [];
            $class_goal_list = ClassGoals::find()
                ->select(['class_id'])
                ->where(['in','goal_id',$goal_ids])
                ->all();
            foreach($class_goal_list as $class_goal){
                $goal_in_query .= "'".$class_goal->class_id."', ";
                $class_goal_ids[] = $class_goal->class_id;
            }
            if (count($class_goal_list) > 0){
                $goal_in_query = substr($goal_in_query, 0, -2);
                $goal_in_query = "(".$goal_in_query.")";
                $additional_brackets = true;
            }




            if ($additional_brackets){
                $where_query.= "(";
            }


            $where_query.= " (t_yoga_classes.name LIKE '%".$search."%' OR t_yoga_classes.description LIKE '%".$search."%' OR t_yoga_classes.description_full LIKE '%".$search."%')";

            if (count($style_list_search) > 0){
                $where_query.= " OR (t_yoga_classes.main_yoga_style in ".$style_in_query.")";
            }

            if (count($teacher_profile_list) > 0){
                $where_query.= " OR (t_yoga_classes.teacher_id in ".$teacher_in_query.")";
            }

            if (count($class_tag_list) > 0){
                $where_query.= " OR (t_yoga_classes.id in ".$tags_in_query.")";
            }

            if (count($class_goal_list) > 0){
                $where_query.= " OR (t_yoga_classes.id in ".$goal_in_query.")";
            }

            if ($additional_brackets){
                $where_query.= ")";
            }
        }

        // ---------- Filter collection ----------

        if ((isset($collection_id)) && ($collection_id != "") && ($collection_id != "0")){



            $collection = CollectionClasses::find()->where("collection_id=".$collection_id)->count();

            if ($collection>0){
                if ($where_query != ""){
                    $where_query.=" AND ";
                }

                //search by style
                $collection_classes_ids = [];
                $collection_list_search = CollectionClasses::find()->select(['class_id'])->where("collection_id = ".$collection_id)->all();

                $collection_in_query = "";
                foreach($collection_list_search as $collection_search){

                    $collection_in_query .= "'".$collection_search->class_id."', ";
                    $collection_classes_ids[] = $collection_search->class_id;
                }
                if (count($collection_list_search) > 0){
                    $collection_in_query = substr($collection_in_query, 0, -2);
                    $collection_in_query = "(".$collection_in_query.")";
                }

                $where_query.= "  (t_yoga_classes.id in ".$collection_in_query.")";
            }

        }


        // ---------- Filter levels ----------


        if ((isset($levels)) && (count($levels)>0) && ($levels[0]!=0)&& ($levels[0]!="")){



            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $where_query.=" (";
            for($j=0;$j<count($levels); $j++){

                if ($levels[$j]!=2){
                    $where_query.="t_yoga_classes.level LIKE '%".$levels[$j]."%'";
                }
                else{
                    $where_query.="t_yoga_classes.level LIKE '%".$levels[$j]."%' OR t_yoga_classes.level LIKE '1-3'";
                }

                if ($j != count($levels)-1){
                    $where_query.= "OR ";
                }

            }
            $where_query.=" )";

        }



        // ---------- Filter duration ----------
        if ((isset($duration)) && (count($duration)>0) && ($duration[0]!=0) && ($duration[0]!="") ){

            if ($where_query != ""){
                $where_query.=" AND ";
            }


            $where_query.=" (";
            for($j=0;$j<count($duration); $j++){

                if ($duration[$j]==10){
                    $where_query.="t_yoga_classes.duration <= 10 ";
                }
                if ($duration[$j]==20){
                    $where_query.="(t_yoga_classes.duration > 10 AND t_yoga_classes.duration <=20) ";
                }
                if ($duration[$j]==30){
                    $where_query.="(t_yoga_classes.duration > 20 AND t_yoga_classes.duration <=30) ";
                }
                if ($duration[$j]==45){
                    $where_query.="(t_yoga_classes.duration > 30 AND t_yoga_classes.duration <=45) ";
                }
                if ($duration[$j]==60){
                    $where_query.="t_yoga_classes.duration > 45 ";
                }




                if ($j != count($duration)-1){
                    $where_query.= "OR ";
                }

            }
            $where_query.=" )";



        }


        // ---------- Filter teacher ----------
        if ((isset($teacher_id)) && ($teacher_id != "") && ($teacher_id != "0")){

            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $where_query.= " teacher_id = ".$teacher_id;
        }

        // ---------- Filter style ----------
        if ((isset($style_id)) && ($style_id != "") && ($style_id != "0")){

            if ($where_query != ""){
                $where_query.=" AND ";
            }

            $where_query.= " main_yoga_style = ".$style_id;
        }


        $total = 0;
        $class_list=null;

        // ---------- NEW ----------
        if ($filter_type == "new"){



            if ($where_query == ""){


                    $total = YogaClasses::find()->andWhere("is_published=1")
                        ->orderBy("create_date DESC")
                        ->count();



            }
            else{


                    $total = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->count();


            }

        }
        // ---------- / NEW ----------



        // ---------- POPULAR ----------
        if ($filter_type == "popular"){

            if ($where_query == ""){

                    $total = YogaClasses::find()->andWhere("is_published=1")
                        ->count();


            }
            else{


                    $total = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->count();


            }

        }
        // ---------- / POPULAR ----------

        // ---------- WATCH LATER ----------
        if ($filter_type == "watch_later"){

            //get watch later list
            $objects_list = UserWishlist::find()->where("user_id=".$user_id)->all();
            $objects_id_array = [];
            foreach($objects_list as $obj){
                $objects_id_array[] = $obj->class_id;
            }

            if ($where_query == ""){

                $total  = YogaClasses::find()->andWhere("is_published=1")
                    ->where(['in','id',$objects_id_array])
                    ->count();


            }
            else{

                $total = YogaClasses::find()->andWhere("is_published=1")
                    ->distinct()
                    ->joinWith("goals")
                    ->where($where_query)
                    ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                    ->count();


            }

        }
        // ---------- / WATCH LATER ----------


        // ---------- FAVORITES ----------
        if ($filter_type == "favorites"){

            //get favorites list
            $objects_list = UserFavorites::find()->where("user_id=".$user_id)->all();
            $objects_id_array = [];
            foreach($objects_list as $obj){
                $objects_id_array[] = $obj->class_id;
            }

            if ($where_query == ""){

                $total = YogaClasses::find()->andWhere("is_published=1")
                    ->where(['in','id',$objects_id_array])
                    ->count();


            }
            else{

                $total = YogaClasses::find()->andWhere("is_published=1")
                    ->distinct()
                    ->joinWith("goals")
                    ->where($where_query)
                    ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                    ->count();


            }

        }
        // ---------- / FAVORITES ----------

        // ---------- HISTORY ----------
        if ($filter_type == "history"){

            //get favorites list
            $objects_list = UserWatched::find()->where("user_id=".$user_id)->all();
            $objects_id_array = [];
            foreach($objects_list as $obj){
                $objects_id_array[] = $obj->class_id;
            }

            if ($where_query == ""){


                $total = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->count();



            }
            else{


                $total = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->count();



            }

        }
        // ---------- / HISTORY ----------

        // ---------- TINDER ----------
        if ($filter_type == "tinder"){

            //get favorites list
            $tinder_class_list = YogaClasses::find()->andWhere("is_published=1")
                ->select(['id'])
                ->orderBy("view_count DESC")
                ->all();

            $tinder_count = 0;
            $objects_id_array = [];

            foreach($tinder_class_list as $tinder_class){

                $is_watched = UserWatched::find()
                    ->where("class_id=".$tinder_class->id)
                    ->andWhere("user_id=".$user_id)
                    ->count();

                if ($is_watched == 0){
                    $objects_id_array[] = $tinder_class->id;
                    $tinder_count++;

                    if ($tinder_count == 10){
                        break;
                    }
                }
            }

            if ($where_query == ""){


                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->where(['in','id',$objects_id_array])
                        ->orderBy("create_date DESC")
                        ->count();



            }
            else{


                    $class_list = YogaClasses::find()->andWhere("is_published=1")
                        ->distinct()
                        ->joinWith("goals")
                        ->where($where_query)
                        ->andWhere(['in','t_yoga_classes.id',$objects_id_array])
                        ->orderBy("t_yoga_classes.create_date DESC ")
                        ->count();



            }

        }
        // ---------- / TINDER ----------


        $page_count = ceil($total/$page_size);

        $return_object = new \stdClass();
        $return_object->page_count = $page_count;



        return $return_object;


    }


    /**
     * @api {get} /yoga-classes/find_by_seo_url find by seo url
     * @apiDescription Find yoga class by seo url
     *
     * @apiName yoga_classes_find_by_seo_url
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {String} url  Yoga class seo url
     * @apiParamExample {json} Request-Example:
     *     {
     *       "url": "Vinyasa-Flow-For-Beginners"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /yoga-classes/find_by_seo_url
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFind_by_seo_url($url){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $class_list = YogaClasses::find()->andWhere("is_published=1")
            ->where("seo_url='".$url."'");


        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);


    }


    /**
     * @api {get} /yoga-classes/set_reminder set reminder
     * @apiDescription Set reminder for yoga class
     *
     * @apiName yoga_classes_set_reminder
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} days_count  Days count to show reminder
     * @apiParam {Number} class_id  Yoga class id
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /yoga-classes/set_reminder
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionSet_reminder(){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $days_count = App::getRequestData("days_count",false);
        $class_id = App::getRequestData("class_id",false);
        YogaHttpException::checkIntParam($days_count, "days_count");
        YogaHttpException::checkIntParam($class_id, "class_id");

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

       //delete old
       ClassReminder::deleteAll("class_id=".$class_id." AND user_id=".$cur_user->id);

        $reminder = new ClassReminder();
        $reminder->user_id = $cur_user->id;
        $reminder->class_id = $class_id;
        $reminder->remind_days = $days_count;
        $reminder->create_date = date("Y-m-d h:i:s");
        $reminder->save();

        die;

    }




    /**
     * @api {get} /yoga-classes/get_popular_by_page get popular by page
     * @apiDescription Get popular yoga classes by page
     *
     * @apiName yoga_classes_get_popular_by_page
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_popular_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_popular_by_page($page=1, $full_list=0, $page_size=10){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->orderBy("view_count DESC")
                ->limit($page_size*$page);



        }
        else{
            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->orderBy("view_count DESC")
                ->offset($page_size*($page-1))
                ->limit($page_size);


        }


        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);



    }

    /**
     * @api {get} /yoga-classes/get_new_by_page get new by page
     * @apiDescription Get new yoga classes by page
     *
     * @apiName yoga_classes_get_new_by_page
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_new_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_new_by_page($page, $full_list, $page_size=10){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->orderBy("create_date DESC")
                ->limit($page_size*$page);

        }
        else{
            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->orderBy("create_date DESC")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);


    }



    /**
     * @api {get} /yoga-classes/get_page_count  get page count
     * @apiDescription Get all yoga classes page count. Can use for calculate page count for new and popular yoga classes
     *
     * @apiName yoga_classes_get_page_count
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} page_size Count of objects per page
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_page_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_page_count($page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $total = YogaClasses::find()->andWhere("is_published=1")->count();
        $page_count = ceil($total/$page_size);



        $return_obj = new \stdClass();
        $return_obj->page_count = $page_count;

        return $return_obj;

    }

    /**
     * @api {get} /yoga-classes/get_tinder get tinder
     * @apiDescription Get tinder yoga classes
     *
     * @apiName yoga_classes_get_tinder
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_tinder
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_tinder(){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();



        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);


        $class_list = YogaClasses::find()->andWhere("is_published=1")
            ->orderBy("view_count DESC")
            ->all();

        $tinder_count = 0;
        $tinder_id_array = [];

        foreach($class_list as $class){

            $is_watched = UserWatched::find()
                ->where("class_id=".$class->id)
                ->andWhere("user_id=".$cur_user->id)
                ->count();

            if ($is_watched == 0){
                $tinder_id_array[] = $class->id;
                $tinder_count++;

                if ($tinder_count == 10){
                    break;
                }
            }
        }

        shuffle($tinder_id_array);


        //for sort in same order as in "IN Array"
        $exp = new Expression('FIELD (id,' . implode(',', $tinder_id_array) . ')');
        $class_list = YogaClasses::find()->andWhere("is_published=1")
            ->where(['in','id',$tinder_id_array])
            ->orderBy([$exp]);



        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);



    }




    /**
     * @api {get} /yoga-classes/search search
     * @apiDescription Search yoga classes by page
     *
     * @apiName yoga_classes_search
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {String} search  Search query
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/search
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionSearch($page=1, $full_list=0, $page_size=10, $search){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkRequiredParam($search, "search");



        //search by style
        $style_ids = [];
        $style_list = YogaStyles::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
        foreach($style_list as $style){
            $style_ids[] = $style->id;
        }

        //search by teacher
        $teacher_ids = [];


        $teacher_user_ids = [];
        $teacher_user_list = User::find()
            ->select(['id'])
            ->where("role = 2")->all();

        foreach($teacher_user_list as $user){
            $teacher_user_ids[] = $user->id;
        }

        $teacher_profile_list =  Profile::find()
            ->select(['user_id'])
            ->where("first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%'")
            ->andWhere(['in','user_id',$teacher_user_ids])
            ->all();

        foreach($teacher_profile_list as $profile){
            $teacher_ids[] = $profile->user_id;
        }

        //search by tags
        $tag_ids = [];
        $tag_list = Tags::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
        foreach($tag_list as $tag){
            $tag_ids[] = $tag->id;
        }

        $class_tag_ids = [];
        $class_tag_list = ClassTags::find()
            ->select(['class_id'])
            ->where(['in','tag_id',$tag_ids])
            ->all();
        foreach($class_tag_list as $class_tag){
            $class_tag_ids[] = $class_tag->class_id;
        }

        //search by goals
        $goal_ids = [];
        $goal_list = Goals::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
        foreach($goal_list as $goal){
            $goal_ids[] = $goal->id;
        }

        $class_goal_ids = [];
        $class_goal_list = ClassGoals::find()
            ->select(['class_id'])
            ->where(['in','goal_id',$goal_ids])
            ->all();
        foreach($class_goal_list as $class_goal){
            $class_goal_ids[] = $class_goal->class_id;
        }


        if ($full_list){
            $search_list = YogaClasses::find()->andWhere("is_published=1")
                ->where("name LIKE '%".$search."%' OR description LIKE '%".$search."%' OR description_full LIKE '%".$search."%'")
                ->orWhere(['in','main_yoga_style',$style_ids])
                ->orWhere(['in','teacher_id',$teacher_ids])
                ->orWhere(['in','id',$class_tag_ids])
                ->orWhere(['in','id',$class_goal_ids])
                ->limit($page*$page_size);

        }
        else{

            $search_list = YogaClasses::find()->andWhere("is_published=1")
                ->where("name LIKE '%".$search."%' OR description LIKE '%".$search."%' OR description_full LIKE '%".$search."%'")
                ->orWhere(['in','main_yoga_style',$style_ids])
                ->orWhere(['in','teacher_id',$teacher_ids])
                ->orWhere(['in','id',$class_tag_ids])
                ->orWhere(['in','id',$class_goal_ids])
                ->offset($page_size*($page-1))
                ->limit($page_size);

        }


        return new ActiveDataProvider([
            'query' => $search_list,
            'pagination' => false,
        ]);


    }


    /**
     * @api {get} /yoga-classes/search_page_count search page count
     * @apiDescription Page count for search functions
     *
     * @apiName yoga_classes_search_page_count
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {String} search  Search query
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/search_page_count
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionSearch_page_count($page=1, $full_list=0, $page_size=10, $search){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkRequiredParam($search, "search");



        //search by style
        $style_ids = [];
        $style_list = YogaStyles::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
        foreach($style_list as $style){
            $style_ids[] = $style->id;
        }

        //search by teacher
        $teacher_ids = [];


        $teacher_user_ids = [];
        $teacher_user_list = User::find()
            ->select(['id'])
            ->where("role = 2")->all();

        foreach($teacher_user_list as $user){
            $teacher_user_ids[] = $user->id;
        }

        $teacher_profile_list =  Profile::find()
            ->select(['user_id'])
            ->where("first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%'")
            ->andWhere(['in','user_id',$teacher_user_ids])
            ->all();

        foreach($teacher_profile_list as $profile){
            $teacher_ids[] = $profile->user_id;
        }

        //search by tags
        $tag_ids = [];
        $tag_list = Tags::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
        foreach($tag_list as $tag){
            $tag_ids[] = $tag->id;
        }

        $class_tag_ids = [];
        $class_tag_list = ClassTags::find()
            ->select(['class_id'])
            ->where(['in','tag_id',$tag_ids])
            ->all();
        foreach($class_tag_list as $class_tag){
            $class_tag_ids[] = $class_tag->class_id;
        }

        //search by goals
        $goal_ids = [];
        $goal_list = Goals::find()->select(['id'])->where("name LIKE '%".$search."%'")->all();
        foreach($goal_list as $goal){
            $goal_ids[] = $goal->id;
        }

        $class_goal_ids = [];
        $class_goal_list = ClassGoals::find()
            ->select(['class_id'])
            ->where(['in','goal_id',$goal_ids])
            ->all();
        foreach($class_goal_list as $class_goal){
            $class_goal_ids[] = $class_goal->class_id;
        }




        $total = YogaClasses::find()->andWhere("is_published=1")
            ->where("name LIKE '%".$search."%' OR description LIKE '%".$search."%' OR description_full LIKE '%".$search."%'")
            ->orWhere(['in','main_yoga_style',$style_ids])
            ->orWhere(['in','teacher_id',$teacher_ids])
            ->orWhere(['in','id',$class_tag_ids])
            ->orWhere(['in','id',$class_goal_ids])
            ->count();





        $page_count = ceil($total/$page_size);


        $return_obj = new \stdClass();
        $return_obj->page_count = $page_count;

        return $return_obj;



    }


    /**
     * @api {get} /yoga-classes/search_by_style search by style
     * @apiDescription Search yoga classes by query and style_id
     *
     * @apiName yoga_classes_search_by_style
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} search  Search query
     * @apiParam {Number} style_id  Style id
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/search_by_style
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionSearch_by_style($style_id, $search){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkRequiredParam($search, "search");
        YogaHttpException::checkIntParam($style_id, "style_id", true);

        $search_list = YogaClasses::find()->andWhere("is_published=1")
            ->where("name LIKE '%".$search."%' OR description LIKE '%".$search."%' OR description_full LIKE '%".$search."%'")
            ->andWhere("main_yoga_style=".$style_id);


        return new ActiveDataProvider([
            'query' => $search_list,
            'pagination' => false,
        ]);




    }


    /**
     * @api {get} /yoga-classes/search_by_teacher search by teacher
     * @apiDescription Search yoga classes by query and teacher_id
     *
     * @apiName yoga_classes_search_by_teacher
     * @apiGroup Yoga Classes
     *
     * @apiParam {String} search  Search query
     * @apiParam {Number} teacher_id  Teacher id
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/search_by_teacher
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionSearch_by_teacher($teacher_id,$search){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkRequiredParam($search, "search");
        YogaHttpException::checkIntParam($teacher_id, "teacher_id", true);


        $search_list = YogaClasses::find()->andWhere("is_published=1")
            ->where("name LIKE '%".$search."%' OR description LIKE '%".$search."%' OR description_full LIKE '%".$search."%'")
            ->andWhere("teacher_id=".$teacher_id);

        return new ActiveDataProvider([
            'query' => $search_list,
            'pagination' => false,
        ]);



    }


    /**
     * @api {get} /yoga-classes/get_by_id get yoga class by id
     * @apiDescription Get yoga class by id
     *
     * @apiName yoga_classes_get_by_id
     * @apiGroup Yoga Classes
     *
     * @apiParam {Number} class_id  class id
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "class_id": 22
     *     }
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /yoga-classes/get_by_id
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_by_id($class_id){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($class_id, "class_id", true);

        $object_list = YogaClasses::find()->andWhere("is_published=1")
            ->where("id=".$class_id);

        $object_count = YogaClasses::find()->andWhere("is_published=1")
            ->where("id=".$class_id)
            ->count();

        if ($object_count == 0){
            throw new YogaHttpException(404,"Yoga class not found");
        }


        return new ActiveDataProvider([
            'query' => $object_list,
            'pagination' => false,
        ]);

    }



}