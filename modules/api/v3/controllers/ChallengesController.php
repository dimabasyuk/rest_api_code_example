<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\ChallengeReminder;
use app\models\Challenges;
use app\models\ChallengeStat;
use app\models\ClassComments;
use app\models\UserFollowingChallenges;
use dektrium\user\models\Profile;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use dektrium\user\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\Expression;
use yii\rest\ActiveController;
use yii\web\Response;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');



class ChallengesController extends ActiveController
{
    public $modelClass = 'app\models\Challenges';


    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {


        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }


    /**
     * @api {get} /challenges get all challenges by page
     * @apiDescription Get all challenges
     *
     * @apiName challenges
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "comments,is_follow"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionIndex($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $objects_list = Challenges::find()->andWhere("is_published=1")
                ->limit($page_size*$page);
        }
        else{

            $objects_list = Challenges::find()->andWhere("is_published=1")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $objects_list,
            'pagination' => false,
        ]);

    }

    /**
     * @api {get} /challenges/get_followed_challenges get followed challenges
     * @apiDescription get followed challenges for user
     *
     * @apiName challenges_get_followed_challenges
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [user_id]  User ID for which you want to get the profile information. If this parameter is not
     * specified - the profile information will be returned for the current user
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /challenges/get_followed_challenges
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_followed_challenges($user_id = -1)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");

        if ($user_id == -1){
            $cur_user =  User::findOne(['auth_key' => $access_token]);

            $user_id = $cur_user->id;
        }



        $follows = ChallengeStat::find()
            ->where("user_id=".$user_id)
            ->all();


        $user_ids = [];

        foreach($follows as $part){
            $user_ids[] = $part->challenge_id;
        }

        $users_list = Challenges::find()->andWhere("is_published=1")
            ->where(['in','id',$user_ids]);


        return new ActiveDataProvider([
            'query' => $users_list,
            'pagination' => false,
        ]);


    }


    /**
     * @api {get} /challenges/follow_challenge follow challenge
     * @apiDescription Follow challenge
     *
     * @apiName challenges_follow_challenge
     * @apiGroup Challenges
     *
     * @apiParam {Number} challenge_id  Challenge id for which you want to follow
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /challenges/follow_challenge
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFollow_challenge($challenge_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        //check if follow exist
        $is_follow_yet = UserFollowingChallenges::find()->andWhere("is_published=1")
            ->where("user_id=".$cur_user->id." AND challenge_id=".$challenge_id)
            ->one();

        if (!isset($is_follow_yet)){
            //create link
            $user_follow_teacher = new UserFollowingChallenges();
            $user_follow_teacher->user_id = $cur_user->id;
            $user_follow_teacher->challenge_id = $challenge_id;
            $user_follow_teacher->save();
            die;
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already followed for this challenge");
        }



    }


    /**
     * @api {get} /challenges/unfollow_challenge unfollow challenge
     * @apiDescription Unfollow challenge
     *
     * @apiName challenge_unfollow_challenge
     * @apiGroup Challenges
     *
     * @apiParam {Number} challenge_id  Challenge id for which you want to unfollow
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /challenges/unfollow_challenge
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionUnfollow_challenge($challenge_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if follow exist
        $is_follow_yet = UserFollowingChallenges::find()->andWhere("is_published=1")
            ->where("user_id=".$cur_user->id." AND challenge_id=".$challenge_id)
            ->one();

        if (isset($is_follow_yet)){
            $is_follow_yet->delete();
            die;
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already unfollowed for this challenge");
        }


    }



    /**
     * @api {get} /challenges/find_by_seo_url find by seo url
     * @apiDescription Find challenge by seo url
     *
     * @apiName challenges_find_by_seo_url
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {String} url  Teacher seo url
     * @apiParamExample {json} Request-Example:
     *     {
     *       "url": "back-to-yoga"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /challenges/find_by_seo_url
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFind_by_seo_url($url){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $class_list = Challenges::find()->andWhere("is_published=1")
            ->where("seo_url='".$url."'");



        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);


    }




    /**
     * @api {get} /challenges/get_participants_by_page get participants by page
     * @apiDescription Get participants by page
     *
     * @apiName challenges_get_participants_by_page
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [sort_type]  Sort type: "asc" or "desc"
     * @apiParam {Number} challenge_id  Challenge id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_participants_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_participants_by_page($page=1, $full_list=0, $page_size=10,  $challenge_id)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);

        $part_rating = ChallengeStat::find()->where("challenge_id=".$challenge_id)->orderBy("watch_seconds DESC")->all();
        $users_ids = [];

        foreach($part_rating as $part){
            $users_ids[] = $part->user_id;
        }

        //for sort in same order as in "IN Array"
        $exp = new Expression('FIELD (user_id,' . implode(',', $users_ids) . ')');

        if ($full_list){




            $users_list = Profile::find()
                ->where(['in','user_id',$users_ids])
                ->orderBy([$exp])
                ->limit($page_size*$page);


        }
        else{

            $users_list = Profile::find()
                ->where(['in','user_id',$users_ids])
                ->orderBy([$exp])
                ->offset($page_size*($page-1))
                ->limit($page_size);

        }




        return new ActiveDataProvider([
            'query' => $users_list,
            'pagination' => false,
            'sort'=> ['defaultOrder' => ['challenge_seconds'=>SORT_DESC]]
        ]);



    }



    /**
     * @api {get} /challenges/get_comments_by_page get comments by page
     * @apiDescription Get challenge comments by page
     *
     * @apiName challenges_get_comments_by_page
     * @apiGroup Challenges
     *
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [sort_type]  Sort type: "asc" or "desc"
     * @apiParam {Number} challenge_id  Challenges id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_comments_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_comments_by_page($page=1, $full_list=0, $page_size=10,  $sort_type = "asc", $challenge_id){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $sort_type = App::setDefaultIfEmpty($sort_type,"asc");


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);

        $sort_int = SORT_ASC;
        if ( strtolower($sort_type) == "desc") {
            $sort_int = SORT_DESC;
        }

       // $page_size = 10;
        if ($full_list){

            $comment_list = ClassComments::find()
                    ->where("comment_type=1 AND class_id=".$challenge_id)
                    ->orderBy(['id'=>$sort_int])
                    ->limit($page_size*$page)
                    ->all();

        }
        else{
            $comment_list = ClassComments::find()
                ->where("comment_type=1 AND class_id=".$challenge_id)
                ->orderBy(['id'=>$sort_int])
                ->offset($page_size*($page-1))
                ->limit($page_size)
                ->all();
        }

        foreach ($comment_list as $comment){


            $comment_user = Profile::find()->where("user_id=".$comment->author_id)->one();
            $comment->username = $comment_user->first_name." ".$comment_user->last_name;
            $comment->address = $comment_user->address;
            $comment->ava = $comment_user->ava_photo;
            $comment->practice_since = $comment_user->practice_since;
        }

       return $comment_list;

    }


    /**
     * @api {get} /challenges/add_comment add comment
     * @apiDescription Add comment to challenge
     *
     * @apiName challenges_add_comment
     * @apiGroup Challenges
     *
     * @apiParam {Number} challenge_id  Challenges id
     * @apiParam {Number} comment_text  Comment text
     * @apiParam {Number} rating  user rating of yoga class in range from 0 to 5
     * @apiParam {Number} [parent_id]  Comment parent id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     *
     * @apiSuccessExample {json} Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "comment_id": 801
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/add_comment
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionAdd_comment($challenge_id,$comment_text,$parent_id=0, $rating=5){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $parent_id = App::setDefaultIfEmpty($parent_id,0);
        $rating = App::setDefaultIfEmpty($rating,5);


        //check params
        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);
        YogaHttpException::checkRequiredParam($comment_text, "comment_text");
        YogaHttpException::checkRangeParam($rating, "rating",0,5,true);
        YogaHttpException::checkIntParam($parent_id, "parent_id");



        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);


        $cur_challenge = Challenges::find()->where("is_published=1")->andWhere("id = ".$challenge_id)->one();
        if (!isset($cur_challenge)){
            throw new YogaHttpException(404,"Challenge not found");
        }

        $transaction = \Yii::$app->db->beginTransaction();

        try{

            $new_comment = new ClassComments();
            $new_comment->class_id = $challenge_id;
            $new_comment->parent_id = $parent_id;
            $new_comment->author_id = $cur_user->id;
            $new_comment->like_count = 0;
            $new_comment->text = $comment_text;
            $new_comment->create_date = date("Y-m-d H:i:s");
            $new_comment->comment_type=1;
            $new_comment->rating=$rating;
            $new_comment->save();

            // add rating to comment
            if ($rating != 0){
                $cur_challenge->sum_review_rating += $rating;
                $cur_challenge->review_count +=1;
                $cur_challenge->save();
            }


            $transaction->commit();


        }catch(Exception $e) {
            throw new YogaHttpException(417,"db transaction error");
        }

        $return_object = new \stdClass();
        $return_object->comment_id = $new_comment->id;

        return $return_object;
    }



    /**
     * @api {get} /challenges/remind_for_challenge remind for challenge
     * @apiDescription Remind for challenge
     *
     * @apiName challenges_remind_for_challenge
     * @apiGroup Challenges
     *
     * @apiParam {Number} challenge_id  Challenges id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/remind_for_challenge
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionRemind_for_challenge($challenge_id){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);


        $new_reminder = new ChallengeReminder;
        $new_reminder->user_id = $cur_user->id;
        $new_reminder->challenge_id = $challenge_id;
        $new_reminder->save();

        die;
    }




    /**
     * @api {get} /challenges/get_active_by_page get active challenges by page
     * @apiDescription Get active challenges
     *
     * @apiName challenges_get_active_by_page
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "comments,is_follow"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_active_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_active_by_page($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");

        if ($full_list){

            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_start < NOW() AND date_finish > NOW()")
                ->limit($page_size*$page);

        }
        else{
            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_start < NOW() AND date_finish > NOW()")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $obj_list,
            'pagination' => false,
        ]);

    }


    /**
     * @api {get} /challenges/get_upcoming_by_page get upcoming challenges by page
     * @apiDescription Get upcoming challenges
     *
     * @apiName challenges_get_upcoming_by_page
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "comments,is_follow"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_upcoming_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_upcoming_by_page($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");

        if ($full_list){

            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_start > NOW()")
                ->limit($page_size*$page);

        }
        else{
            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_start > NOW() ")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $obj_list,
            'pagination' => false,
        ]);



    }


    /**
     * @api {get} /challenges/get_past_by_page get past challenges by page
     * @apiDescription Get past challenges
     *
     * @apiName challenges_get_past_by_page
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "comments,is_follow"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_past_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_past_by_page($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_finish < NOW()")
                ->limit($page_size*$page);

        }
        else{
            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_finish < NOW() ")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }



        return new ActiveDataProvider([
            'query' => $obj_list,
            'pagination' => false,
        ]);



    }



    /**
     * @api {get} /challenges/get_ongoing_by_page get ongoing challenges by page
     * @apiDescription Get active and upcoming challenges
     *
     * @apiName challenges_get_ongoing_by_page
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "comments,is_follow"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_ongoing_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_ongoing_by_page($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");



        if ($full_list){

            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_finish > NOW()")
                ->limit($page_size*$page);

        }
        else{
            $obj_list = Challenges::find()
                ->where("is_published=1")
                ->andWhere("date_finish > NOW()")
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }



        return new ActiveDataProvider([
            'query' => $obj_list,
            'pagination' => false,
        ]);



    }

    /**
     * @api {get} /challenges/get_by_id get challenge by id
     * @apiDescription Get challenge by id
     *
     * @apiName challenges_get_by_id
     * @apiGroup Challenges
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} challenge_id  challenge id
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *       "challenge_id": 5
     *     }
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /challenges/get_by_id
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_by_id($challenge_id){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check params
        YogaHttpException::checkIntParam($challenge_id, "challenge_id", true);

        $objects_list = Challenges::find()->andWhere("is_published=1")
            ->where("id=".$challenge_id);

        $object_count = $objects_list->count();

        if ($object_count == 0){
            throw new YogaHttpException(404,"Challenge not found");
        }


        return new ActiveDataProvider([
            'query' => $objects_list,
            'pagination' => false,
        ]);

    }


}


