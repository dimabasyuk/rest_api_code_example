<?php
namespace app\modules\api\v3\controllers;


use app\exception\YogaHttpException;
use app\helpers\App;
use app\models\UserFollowingTeachers;
use app\models\YogaClasses;
use app\models\YogaStyles;
use dektrium\user\models\Profile;
use dektrium\user\models\User;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\rest\ActiveController;


header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, access-token');
header('Access-Control-Allow-Methods: PUT,GET,HEAD,POST,PATCH,DELETE,OPTIONS');



class TeachersController extends ActiveController
{

    public $modelClass = '\dektrium\user\models\Profile';

    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD', 'OPTIONS'],
            'view' => ['GET', 'HEAD'],
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
        ];
    }

    public function actions()
    {


        $actions = parent::actions();
        unset($actions['index']);
        return $actions;
    }


    /**
     * @api {get} /teachers get all teachers by page
     * @apiDescription Get all teachers
     *
     * @apiName teachers
     * @apiGroup Teachers
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "geo,statistic"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /teachers
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionIndex($page=1, $full_list=0, $page_size=10){


        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        
        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);



        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");


        //get teachers list
        $teacher_list = User::find()->select("id")->where("role=2")->orderBy("sort_order ASC")->all();
        $teacher_id_array = [];
        foreach($teacher_list as $teacher){
            $teacher_id_array[] = $teacher->id;
        }

        //for sort in same order as in "IN Array"
        $exp = new Expression('FIELD (user_id,' . implode(',', $teacher_id_array) . ')');

        if ($full_list){

            $object_list = Profile::find()
                ->where(['in','user_id',$teacher_id_array])
                ->orderBy([$exp])
                ->limit($page_size*$page);
        }
        else{

            $object_list = Profile::find()
                ->where(['in','user_id',$teacher_id_array])
                ->orderBy([$exp])
                ->offset($page_size*($page-1))
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $object_list,
            'pagination' => false,
        ]);

    }

    /**
     * @api {get} /teachers/get_teacher_classes_by_page get teacher classes by page
     * @apiDescription Get teacher classes
     *
     * @apiName teachers_get_teacher_classes_by_page
     * @apiGroup Teachers
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [page]  Start page
     * @apiParam {Number} [full_list]  If <b>full_list = 1</b> will be returned objects from the first page to the
     * selected page. <br/> If <b>full_list = 0</b> will be returned objects only for the selected page.
     * @apiParam {Number} [page_size]  Count of objects per page
     * @apiParam {Number} [sort_type]  Sort type: "new" or "popular"
     * @apiParam {Number} teacher_id  Teachers id for which you want to get data
     * @apiParamExample {json} Request-Example:
     *     {
     *       "expand": "style,goals",
     *       "teacher_id": 14
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     *
     * @apiSampleRequest /teachers/get_teacher_classes_by_page
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_teacher_classes_by_page($page=1, $full_list=0, $page_size=10, $sort_type="new", $teacher_id)
    {

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check if documentation server send empty string
        $page = App::setDefaultIfEmpty($page,1);
        $full_list = App::setDefaultIfEmpty($full_list,0);
        $page_size = App::setDefaultIfEmpty($page_size,10);
        $sort_type = App::setDefaultIfEmpty($sort_type,"new");


        //check params
        YogaHttpException::checkIntParam($page, "page");
        YogaHttpException::checkRangeParam($full_list, "full_list", 0 , 1);
        YogaHttpException::checkIntParam($page_size, "page_size");
        YogaHttpException::checkIntParam($teacher_id, "teacher_id", true);


        $order = "create_date DESC";
        if ($sort_type != "new"){
            $order = "view_count DESC";
        }


        if ($full_list){

            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where("teacher_id=".$teacher_id)
                ->orderBy($order)
                ->limit($page_size*$page);

        }
        else{
            $class_list = YogaClasses::find()->andWhere("is_published=1")
                ->where("teacher_id=".$teacher_id)
                ->offset($page_size*($page-1))
                ->orderBy($order)
                ->limit($page_size);
        }

        return new ActiveDataProvider([
            'query' => $class_list,
            'pagination' => false,
        ]);

    }


    /**
     * @api {get} /teachers/follow_teacher follow teacher
     * @apiDescription Follow teacher
     *
     * @apiName teachers_follow_teacher
     * @apiGroup Teachers
     *
     * @apiParam {Number} teacher_id  Teachers id for which you want to follow
     * @apiParamExample {json} Request-Example:
     *     {
     *       "teacher_id": 14
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /teachers/follow_teacher
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFollow_teacher($teacher_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkIntParam($teacher_id, "teacher_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        //check if follow exist
        $is_follow_yet = UserFollowingTeachers::find()
            ->where("user_id=".$cur_user->id." AND teacher_id=".$teacher_id)
            ->one();

        if (!isset($is_follow_yet)){
            //create link
            $user_follow_teacher = new UserFollowingTeachers();
            $user_follow_teacher->user_id = $cur_user->id;
            $user_follow_teacher->teacher_id = $teacher_id;
            $user_follow_teacher->save();
            die;
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already followed for this teacher");
        }



    }

    /**
     * @api {get} /teachers/unfollow_teacher unfollow teacher
     * @apiDescription Unfollow teacher
     *
     * @apiName teachers_unfollow_teacher
     * @apiGroup Teachers
     *
     * @apiParam {Number} teacher_id  Teachers id for which you want to unfollow
     * @apiParamExample {json} Request-Example:
     *     {
     *       "teacher_id": 14
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /teachers/unfollow_teacher
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionUnfollow_teacher($teacher_id)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();


        //check params
        YogaHttpException::checkIntParam($teacher_id, "teacher_id", true);

        //get current user
        $cur_user = User::findOne(['auth_key' => $access_token]);

        // check if follow exist
        $is_follow_yet = UserFollowingTeachers::find()
            ->where("user_id=".$cur_user->id." AND teacher_id=".$teacher_id)
            ->one();

        if (isset($is_follow_yet)){
            $is_follow_yet->delete();
            die;
        }
        else{
            //417 error - Expectation Failed
            throw new YogaHttpException(417,"Expectation Failed: user is already unfollowed for this teacher");
        }


    }


    /**
     * @api {get} /teachers/find_by_seo_url find by seo url
     * @apiDescription Find teacher by seo url
     *
     * @apiName teachers_find_by_seo_url
     * @apiGroup Teachers
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {String} url  Teacher seo url
     * @apiParamExample {json} Request-Example:
     *     {
     *       "url": "jenniferpansa"
     *     }
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /teachers/find_by_seo_url
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionFind_by_seo_url($url){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $profile = Profile::find()
            ->where("seo_url='".$url."'");

        return new ActiveDataProvider([
            'query' => $profile,
            'pagination' => false,
        ]);


    }


    /**
     * @api {get} /teachers/get_followed_teachers get followed teachers
     * @apiDescription get followed teachers for user
     *
     * @apiName teachers_get_followed_teachers
     * @apiGroup Teachers
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} [user_id]  User ID for which you want to get the profile information. If this parameter is not
     * specified - the profile information will be returned for the current user
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /teachers/get_followed_teachers
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionGet_followed_teachers($user_id = -1)
    {
        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        //check if documentation server send empty string
        $user_id = App::setDefaultIfEmpty($user_id,-1);


        //check params
        YogaHttpException::checkIntParam($user_id, "user_id");

        if ($user_id == -1){
            $cur_user =  User::findOne(['auth_key' => $access_token]);

            $user_id = $cur_user->id;
        }



        $follows = UserFollowingTeachers::find()
            ->where("user_id=".$user_id)
            ->all();


        $user_ids = [];

        foreach($follows as $part){
            $user_ids[] = $part->teacher_id;
        }

        $users_list = Profile::find()
            ->where(['in','user_id',$user_ids]);


        return new ActiveDataProvider([
            'query' => $users_list,
            'pagination' => false,
        ]);


    }


    /**
     * @api {get} /teachers/teachers_search search teacher
     * @apiDescription search teacher
     *
     * @apiName teachers_teachers_search
     * @apiGroup Teachers
     *
     * @apiParam {String} [expand]  Expand model to get more fields. <a href="/#api-_footer">View documentation</a>
     * @apiParam {Number} search  Search query
     *
     *
     * @apiHeader {String} access-token Users unique access-key
     * @apiHeaderExample {json} Header-Example:
     *     {
     *       "access-token": "kVvio8ObK03M9MlIHmtiFq6U65I3Gdrt"
     *     }
     *
     * @apiError 404 User for this access-token not found
     * @apiError 400 Missing required parameter
     * @apiError 417 Action has already been made previously
     *
     * @apiSampleRequest /teachers/teachers_search
     *
     * @apiVersion 3.0.0
     *
     */
    public function actionTeachers_search(){

        //check and get access_token
        $access_token = YogaHttpException::checkAndGetToken();

        $search = App::getRequestData("search",false);

        YogaHttpException::checkRequiredParam($search, "search");

        //get teachers id
        $teachers_ids = [];
        $teachers_list = User::find()
            ->where("role = 2")
            ->all();
        foreach($teachers_list as $teacher){
            $teachers_ids[] = $teacher->id;
        }



        //search by style
        $style_ids = [];
        $style_list = YogaStyles::find()->where("name LIKE '%".$search."%'")->all();
        foreach($style_list as $style){
            $style_ids[] = $style->id;
        }


        $search_profile = Profile::find()
            ->where("first_name LIKE '%".$search."%' OR last_name LIKE '%".$search."%'")
            ->orWhere(['in','main_yoga_style',$style_ids])
            ->andWhere(['in','user_id',$teachers_ids]);



        return new ActiveDataProvider([
            'query' => $search_profile,
            'pagination' => false,
        ]);
    }






}