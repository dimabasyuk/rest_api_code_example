<?php

namespace app\modules\api;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\api\controllers';

    public function init()
    {
        parent::init();

        // api versions initialization
        $this->modules = [
            'v2' => [
                'class' => 'app\modules\api\v2\Module',
            ],
            'v3' => [
                'class' => 'app\modules\api\v3\Module',
            ],
        ];
    }
}
